<?php
    /**
        flows.php 
            description --> This page permit to display the flows that are in the DB of the application
            Controllers --> None
            Model --> None
    **/

	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	
	require_once '../Config/BD_Conn.php';
	$sql="select name from user where email='".$_SESSION['owner']."'";
	$resultrech = $dbh->query($sql);
	$owner = $resultrech->fetch();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <title>Flows</title>
        
        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="../assets/css/styles.css" />
        
        <!-- Including the Lobster font from Google's Font Directory -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bungee+Shade|Oswald|Suez+One|Yatra+One" rel="stylesheet">

        <link rel="icon" type="image/png" href="../assets/img/2.png" />
        
    </head>
    
    <body>

        <header>
            <div class="logo">
                <img src="../assets/img/1.png" alt="engie">
            </div>
            <div class="flows">
                <h2>Flows</h2>
            </div>
        </header>

        <nav>
            <ul class="ulf">
                <li> <a href="#"><b><?php echo("<b>Welcome ".strtoupper($owner[0])."</b>"); ?></b></a></li>
                <li> <a href="flows.php"><b>Flows</b></a> </li> 
                <li class="aff" id="aff1"> <a href="#"> <img src="../assets/img/a2.png"> <span><b>Add flow</b></span></a> </li>
                <li class="aff" id="aff2"> <a href="#"> <img src="../assets/img/s21.png"> <span><b>Edit flow</b></span></a> </li>
                <li> <a href="middleware.php"><b>Middlewares</b></a> </li>
                <li> <a href="component.php"><b>Components</b></a> </li>
                <li> <a href="partner.php"><b>Partners</b></a> </li>
                <li> <a href="../Controller/logout.php"><b>Logout</b></a> </li>
            </ul>
        </nav>

        <div>
            <div class="display_fl_comp">
                    <table class="table-fill">
                        <thead>
                            <tr>
                                <th class="text-left">Source</th>
                                <th class="text-left">Component N°1</th>
                                <th class="text-left">Middleware</th>
                                <th class="text-left">Component N°2</th>
                                <th class="text-left">Destination</th>
                                <th class="text-left">Environment</th>
                                <th class="text-left" colspan=2 id="man">Manage</th>
                            </tr>
                        </thead>
                        <tbody class="table-hover">
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            



                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>

                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>

                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>

                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                            <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                             <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                             <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                             <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                             <tr data-url="index.html">
                                <td class="text-left">source<b>a</b></td>
                                <td class="text-left">compo1 <b>a</b></td>
                                <td class="text-left">middleware <b>a</b></td>
                                <td class="text-left">compo2 <b>a</b></td>
                                <td class="text-left">destination <b>b</b></td>
                                <td class="text-left">Env <b>x</b></td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/s2.png" alt="manage">
                                    </a>
                                </td>
                                <td class="text-left" id="manage">
                                    <a href="#" class="amanage">
                                        <img src="../assets/img/tr2.png" alt="manage">
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

            </div>

    
  </body>
</html>


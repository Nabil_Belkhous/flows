<?php

	  /**
        ajaxComponent_refresh.php 
            description --> This page allows the autocompletion when we search to edit a component
            Controllers --> addComp_Controller.php
            Model --> addMidd_Model.php
    **/

function connect() {
	require_once '../Config/Config.php';
	return new PDO('mysql:host='.$host.';dbname='.$dbname.'', $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}

$pdo = connect();
$keyword = '%'.$_POST['keyword'].'%';
$sql = "SELECT name FROM component WHERE name LIKE (:keyword) ORDER BY name";
$query = $pdo->prepare($sql);
$query->bindParam(':keyword', $keyword, PDO::PARAM_STR);
$query->execute();
$list = $query->fetchAll();
foreach ($list as $rs) {
	// put in bold the written text
	$name = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $rs['name']);
	// add new option
    echo '<li onclick="set_item(\''.str_replace("'", "\'", $rs['name']).'\')">'.$name.'</li>';
}
?>
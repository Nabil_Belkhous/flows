<?php
   
   /**
        editComponent_error.php 
            description --> Error in the research of a component to edit it
            Controllers --> updateComp_Controller.php
            Model --> None: The model is integrated with the controller because of the small siza of the query code
    **/

	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	
	require_once '../Config/BD_Conn.php';
	$sql="select name from user where email='".$_SESSION['owner']."'";
	$resultrech = $dbh->query($sql);
	$owner = $resultrech->fetch();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <title></title>
        
        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="../assets/css/styles.css" />
        
        <!-- Including the Lobster font from Google's Font Directory -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bungee+Shade|Oswald|Suez+One|Yatra+One" rel="stylesheet">

        <link rel="icon" type="image/png" href="../assets/img/2.png" />
        
        <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/scriptComponent.js"></script>
    </head>
    
    <body>

        <header>
            <div class="logo">
                <img src="../assets/img/1.png" alt="engie">
            </div>
            <div class="flows">
                <h2>Flows</h2>
            </div>
        </header>

        <nav>
            <ul class="ulf">
                <li> <a href="#"><b><?php echo("<b>Welcome ".strtoupper($owner[0])."</b>"); ?></b></a></li>
                <li> <a href="flows.php"><b>Flows</b></a> </li> 
                <li> <a href="middleware.php"><b>Middlewares</b></a> </li>
                <li> <a href="component.php"><b>Components</b></a> </li>
                <li class="aff"> <a href="addComponent.php"> <img src="../assets/img/a2.png"> <span><b>Add component</b></span></a> </li></br>
                <li class="aff" id="aff2"> <a href="editComponent.php"> <img src="../assets/img/s21.png"> <span><b>Edit component</b></span></a> </li>
                <li> <a href="partner.php"><b>Partners</b></a> </li>
                <li> <a href="../Controller/logout.php"><b>Logout</b></a> </li>
            </ul>
        </nav>
        
        <fieldset>
          <legend> Search </legend>
            <form method = "POST" action = "../Controller/updateComp_Controller.php" name = "form_update">
               
               <div class="label_div"><b>Name  : </b></div>
                <div class="input_container">
                    <input type="text" id="mid_name" name="mid" required onkeyup="autocomplet()">
                    <ul id="mid_list_id"></ul>
                </div>
              </br> </br>

              <input type='submit' name='edit' id='edit' value=" "  >
              <input type='submit' name='delete' id='delete' value=" "  >
                  
            </form>
        </fieldset>
        <div class = "Verif_update"> 
          <p><b>The component does't exist</b></p>
        </div>

        <?php
            echo("
                <footer>
                    <div class='bas'>
                        <img src='../assets/img/flux.png'>  
                        <div id='corp'> <p><b>© 2016 ENGIE IT Corporation. All Rights Reserved</b></p> </div>
                    </div>
                </footer>"
            );
        ?>
    
  </body>
</html>


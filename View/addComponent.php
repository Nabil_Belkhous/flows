<?php

  /**
        addComponent.php 
            description --> This page permit add new component
            Controllers --> addComp_Controller.php
            Model --> addComp_Model.php
    **/

	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	
	require_once '../Config/BD_Conn.php';
	$sql="select name from user where email='".$_SESSION['owner']."'";
	$resultrech = $dbh->query($sql);
	$owner = $resultrech->fetch();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <title></title>
        
        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="../assets/css/styles.css" />
        
        <!-- Including the Lobster font from Google's Font Directory -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bungee+Shade|Oswald|Suez+One|Yatra+One" rel="stylesheet">

        <link rel="icon" type="image/png" href="../assets/img/2.png" />
        
    </head>
    
    <body>

        <header>
            <div class="logo">
                <img src="../assets/img/1.png" alt="engie">
            </div>
            <div class="flows">
                <h2>Flows</h2>
            </div>
        </header>

        <nav>
            <ul class="ulf">
                <li> <a href="#"><b><?php echo("<b>Welcome ".strtoupper($owner[0])."</b>"); ?></b></a></li>
                <li> <a href="flows.php"><b>Flows</b></a> </li> 
                <li> <a href="middleware.php"><b>Middlewares</b></a> </li>
                <li> <a href="component.php"><b>Components</b></a> </li>
                <li class="aff"> <a href="addComponent.php"> <img src="../assets/img/a2.png"> <span><b>Add component</b></span></a> </li></br>
                <li class="aff" id="aff2"> <a href="editComponent.php"> <img src="../assets/img/s21.png"> <span><b>Edit component</b></span></a> </li>
                <li> <a href="partner"><b>Partners</b></a> </li>
                <li> <a href="../Controller/logout.php"><b>Logout</b></a> </li>
            </ul>
        </nav>
        

        <fieldset class="add_midd">
            
                <form method = "POST" action = "../Controller/addComp_Controller.php" name = "form_add_comp">
                    <table class="table_add">
                       <tr>
                        <tr>
                          <td class="line"> <label><b>Name</b></label> </td>
                          <td class="line"> <input type="text" name="name_mid"> </td>
                       </tr>
                          <td class="line"> <label><b>Environment</b></label> </td>
                          <td class="line"> 
                            <SELECT name="select_env" size="1" >
                                <OPTION>REC
                                <OPTION>PROD
                                <OPTION>PPROD
                                <OPTION>DEV
                            </SELECT>
                          </td>
                       </tr>
                       <tr>
                          <td class="line"> <label><b>Localisation</b></label> </td>
                          <td class="line"> <input type="text" name="loc"> </td>
                       </tr>
                       <tr>
                          <td class="line"> <label><b>Server name</b></label> </td>
                          <td class="line"> <input type="text" name="name_server"> </td>
                       </tr>
                       <tr>
                          <td class="line"> <label><b>IP Addresses<b></label> </td>
                          <td class="line"> <textarea rows="5" cols="100" name="ip"> </textarea> </td>
                       </tr>
                       <tr>
                          <td class="line"> <label><b>Port</b></label> </td>
                          <td class="line"> <input type="text" name="port"> </td>
                       </tr>
                       <tr>
                          <td class="line"> <label><b>DNS</b></label> </td>
                          <td class="line"> <input type="text" name="dns"> </td>
                       </tr>
                       <tr>
                          <td class="line"> <label><b>Acces account</b></label> </td>
                          <td class="line"> <input type="text" name="account"> </td>
                       </tr>

                    </table>

                    <input type='submit' name='add_c' id='add_c' value="Add"  >
        
                </form>
        </fieldset>
        
        <?php
            echo("
                <footer>
                    <div class='bas'>
                        <img src='../assets/img/flux.png'>  
                        <div id='corp'> <p><b>© 2016 ENGIE IT Corporation. All Rights Reserved</b></p> </div>
                    </div>
                </footer>"
            );
        ?>
    
  </body>
</html>


<?php

    /**
        partner.php 
            description --> Allows to display the partners that are in the DB of the application
            Controllers --> None
            Model --> None
    **/

	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	
	require_once '../Config/BD_Conn.php';
	$sql="select name from user where email='".$_SESSION['owner']."'";
	$resultrech = $dbh->query($sql);
	$owner = $resultrech->fetch();

    /* Upload the partners */

    $sql="select * from partner";
    $resultrech = $dbh->query($sql);
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <title></title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="../assets/css/styles.css" />
        
        <!-- Including the Lobster font from Google's Font Directory -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bungee+Shade|Oswald|Suez+One|Yatra+One" rel="stylesheet">

        <link rel="icon" type="image/png" href="../assets/img/2.png" />
        
    </head>
    
    <body>

        <header>
            <div class="logo">
                <img src="../assets/img/1.png" alt="engie">
            </div>
            <div class="flows">
                <h2>Flows</h2>
            </div>
        </header>

        <nav>
            <ul class="ulf">
                <li> <a href="#"><b><?php echo("<b>Welcome ".strtoupper($owner[0])."</b>"); ?></b></a></li>
                <li> <a href="flows.php"><b>Flows</b></a> </li> 
                <li> <a href="middleware.php"><b>Middlewares</b></a> </li>
                <li> <a href="component.php"><b>Components</b></a> </li>
                <li> <a href="partner.php"><b>Partners</b></a> </li>
                <li class="aff"> <a href="addPartner.php"> <img src="../assets/img/a2.png"> <span><b>Add partner</b></span></a> </li></br>
                <li class="aff" id="aff2"> <a href="editPartner.php"> <img src="../assets/img/s21.png"> <span><b>Edit partner</b></span></a> </li>
                <li> <a href="../Controller/logout.php"><b>Logout</b></a> </li>
            </ul>
        </nav>
        

        <div>
            <div class="display_fl_comp">
                    <table class="table-fill">
                        <thead>
                            <tr>
                                <th class="text-left">Partner</th>
                                <th class="text-left">Environement</th>
                                <th class="text-left">Localisation</th>
                                <th class="text-left">IP address</th>
                                <th class="text-left">Port</th>
                                <th class="text-left">DNS</th>
                                <th class="text-left">Server</th>
                                <th class="text-left">Accounts</th>
                                <th class="text-left" colspan=2 >Manage</th>
                            </tr>
                        </thead>
                        <tbody class="table-hover">

                            <?php
                                while($result = $resultrech->fetch()){
                                    echo ("<tr data-url='index.html'>");
                                        echo ("<td class='text-left'> ".$result[0]."</td>");

                                            /* Getting the description informations */
                                            $sql1="select * from description where iddesc =".$result[1];
                                            $resultrech1 = $dbh->query($sql1);
                                            $res= $resultrech1->fetch();

                                            $sql2="select name from environment where id =".$res[1];
                                            $resultrech2 = $dbh->query($sql2);
                                            $res2= $resultrech2->fetch();

                                            /* Getting the name of the environment *///             

                                        echo ("<td class='text-left'> ".$res2[0]."</td>");
                                        echo ("<td class='text-left'> ".$res[2]."</td>");
                                        echo ("<td class='text-left'> ".$res[3]."</td>");
                                        echo ("<td class='text-left'> ".$res[4]."</td>");
                                        echo ("<td class='text-left'> ".$res[5]."</td>");
                                        echo ("<td class='text-left'> ".$res[6]."</td>");
                                        echo ("<td class='text-left'> ".$res[7]."</td>");
                                        echo("<td class='text-left'  style='text-align: center' id='manage'>");
                                            echo("<a href=\"../Controller/updatePart_table_Controller.php?mid=$result[0]\" class='amanage'>");
                                                echo("<img src='../assets/img/s2.png' alt='manage'>");
                                            echo("</a>");
                                            echo("</td>");
                                        echo("<td class='text-left' style='text-align: center' id='manage'>");
                                            echo("<a href=\"../Controller/deletePart_table_Controller.php?mid=$result[0]\" class='amanage'>");
                                                echo("<img src='../assets/img/tr2.png' alt='manage'>");
                                            echo("</a>");
                                        echo("</td>");
                                    echo("</tr>");
                                }
                            ?>    
                            
                        </tbody>
                    </table>

            </div>
        
    
    
  </body>
</html>


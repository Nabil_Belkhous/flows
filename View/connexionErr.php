<?php
    /**
        ConnexionErr.php 
            description --> View that permit to treat errors authentification
            Controllers --> connexion_Controller.php
            Model --> user_Model.php
    **/

?>


<!DOCTYPE html>
<html>
    <html>
    <head>
        <meta charset="utf-8" />
        
        <title>Flows</title>
        
        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="../assets/css/styles.css" />
        
        <!-- Including the Lobster font from Google's Font Directory -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bungee+Shade|Oswald|Suez+One|Yatra+One" rel="stylesheet">

        <link rel="icon" type="image/png" href="../assets/img/2.png" />
        
    </head>
    
    <body>

        <header>
            <div class="logo">
                <img src="../assets/img/1.png" alt="engie">
            </div>
            <div class="flows">
                <h2>Flows</h2>
            </div>
            
        </header>

        <fieldset>
            <legend> Connexion  </legend>
                <form method = "POST" action = "../Controller/connexion_Controller.php" name = "form_connexion">
                     
                    <div class = "div-ligne">
                        <div class = "div-gauche">
                            <label for='log'>Email</label>
                        </div>
                        <div class = "div-droit">
                            <input type="email" id="log" name="log" maxlength ="70" required>
                        </div>
                    </div>      
                    <div class = "div-ligne">
                        <div class = "div-gauche">
                            <label for='mp'>Password</label>
                        </div>
                        <div class = "div-droit">
                            <input type="password" id="mp" name="mp" maxlength ="30" required> 
                        </div>
                    </div>  

                    <input type='submit' name='conex' id='conex' value=" Connexion"  >
                </form>
                </br><hr/>
                 <div class="dv_img"><a class="search_bt" href="search.html"> <span><b>Search flows</b></span> </a></div> 
                 </br>
        </fieldset>

        <div class="Err_conex">
            <p>Authentification error, please check your email and your password</p>
        </div>

        <footer>
            <div class="bas">
                <img src="../assets/img/flux.png">  
                <div id="corp"> <p><b>© 2016 ENGIE IT Corporation. All Rights Reserved</b></p> </div>
            </div>
        </footer>
            
    </body>
</html>


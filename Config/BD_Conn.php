<?php
	
	/* on inclut les variables de parametrage de la connexion a la base de donnees*/
	require_once 'Config.php';
	
	
	// connexion à Mysql sans base de données
	$dbh = new PDO('mysql:host='.$host, $user, $password);
		 
	// création de la requête sql
	// on teste avant si elle existe ou non (par sécurité)
	/*$requete = "CREATE DATABASE IF NOT EXISTS `".$dbname."` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		 
	// on prépare et on exécute la requête
	$dbh->prepare($requete)->execute();*/

	try
		{
			/* Connexion avec la base de donnees*/
			$dbh = new PDO('mysql:host='.$host.';dbname='.$dbname.'', $user, $password);
			
		}
		catch(PDOException $e)
		{
			/* En cas de probleme avec la connexion de la BD ce message d'erreur s'affichera */
			Die ( 'Erreur  au niveau de la BDD ');
		}
?>

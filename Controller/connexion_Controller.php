<?php
	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	require_once '../Config/BD_Conn.php';
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	/* Recover the submited information */
	$login = trim($_POST['log']) ;
	$password = trim($_POST['mp']) ;
	
	/* see Model */
	include '../Model/user_Model.php';

	if ( $count == 1 )
	{
		/* Open the a new session */
		$_SESSION['owner']=$login;
		/* Redirection into the home page */
		header('Location:../View/flows.php');
	}
	else 
	{
		/* if there are erros: Redirection into the error connexion page */
		header('Location: ../View/connexionErr.php');	
	}

	$resultrech->closeCursor();
		
?>
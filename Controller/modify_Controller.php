<?php
	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	require_once '../Config/BD_Conn.php';
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$name=$_SESSION['dropName'];
	$name_mid = trim($_POST['name_mid']) ;

	
	/* Recover the submited information */
	$env = trim($_POST['select_env']) ;
	$loc = trim($_POST['loc']) ;
	$server = trim($_POST['name_server']) ;
	$ip = trim($_POST['ip']) ;
	$port = trim($_POST['port']) ;
	$dns = trim($_POST['dns']) ;
	$access = trim($_POST['account']) ;
	
	require_once '../Model/modify_Model.php';
	

?>
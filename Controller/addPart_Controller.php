<?php
	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	require_once '../Config/BD_Conn.php';
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	/* Recover the submited information */
	$name = trim($_POST['name_mid']) ;
	$env = trim($_POST['select_env']) ;
	$loc = trim($_POST['loc']) ;
	$server = trim($_POST['name_server']) ;
	$ip = trim($_POST['ip']) ;
	$port = trim($_POST['port']) ;
	$dns = trim($_POST['dns']) ;
	$access = trim($_POST['account']) ;
	
	$_SESSION['middleware'] = $_POST['name_mid'];
	$_SESSION['environment'] = $_POST['select_env'];
	$_SESSION['location'] = $_POST['loc'];
	$_SESSION['server'] = $_POST['name_server'];
	$_SESSION['ip'] = $_POST['ip'];
	$_SESSION['port'] = $_POST['port'];
	$_SESSION['dns'] = $_POST['dns'];
	$_SESSION['acces'] = $_POST['account'];

	/* see Model */
	include '../Model/addPart_Model.php';

?>
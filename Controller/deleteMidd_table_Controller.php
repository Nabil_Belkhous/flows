<?php
	session_start();
	if(!$_SESSION['owner'])
	{
		header('Location: ../index.php');
	}
	require_once '../Config/BD_Conn.php';
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$sql="select name from user where email='".$_SESSION['owner']."'";
	$resultrech = $dbh->query($sql);
	$owner = $resultrech->fetch();

	
	$id  = $_GET["mid"] ;

	if (isset($_POST['confir'])) {

		$selectId = $dbh->query("select id_desc from middleware where name ='".$id."'");
		
		$result1 = $selectId->fetch();
		$dropMidd = $dbh->query("delete from description where iddesc ='".$result1[0]."'");

		$dropMidd = $dbh->query("delete from middleware where name ='".$id."'");
		header('Location: ../View/middleware.php');	

	} else if (isset($_POST['abort'])) {
			header('Location: ../View/middleware.php');	
	}

		
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <title></title>
        
        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="../assets/css/styles.css" />
        
        <!-- Including the Lobster font from Google's Font Directory -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Bungee+Shade|Oswald|Suez+One|Yatra+One" rel="stylesheet">

        <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/script.js"></script>

        <link rel="icon" type="image/png" href="../assets/img/2.png" />
    </head>
    
    <body>

        <header>
            <div class="logo">
                <img src="../assets/img/1.png" alt="engie">
            </div>
            <div class="flows">
                <h2>Flows</h2>
            </div>
        </header>

        <nav>
            <ul class="ulf">
                <li> <a href="#"><b><?php echo("<b>Welcome ".strtoupper($owner[0])."</b>"); ?></b></a></li>
                <li> <a href="flows.html"><b>Flows</b></a> </li> 
                <li> <a href="middleware.php"><b>Middlewares</b></a> </li>
                <li class="aff"> <a href="addMiddleware.php"> <img src="../assets/img/a2.png"> <span><b>Add middleware</b></span></a> </li> </br>
                <li class="aff" id="aff2"> <a href="updateMiddleware.php"> <img src="../assets/img/s21.png"> <span><b>Edit middleware</b></span></a> </li>
                <li> <a href="#"><b>Components</b></a> </li>
                <li> <a href="#"><b>Partners</b></a> </li>
                <li> <a href="../Controller/logout.php"><b>Logout</b></a> </li>
            </ul>
        </nav>
        
        <div class="Etat_gestion">
    
            <form method = "POST" action = "#" name = "form_delete_midd">
                 <div class="confirm">
                    <p>Do you really want to delete this middleware ?</p>
                    <input type='submit' name='confir' id='confir' value="Confirm"> 
                    <input type='submit' name='abort' id='abort' value="Abort"> 
                </div>
                
            </form>     

        </div>

        <?php
            echo("
                <footer>
                    <div class='bas'>
                        <img src='../assets/img/flux.png'>  
                        <div id='corp'> <p><b>© 2016 ENGIE IT Corporation. All Rights Reserved</b></p> </div>
                    </div>
                </footer>"
            );
        ?>
    
  </body>
</html>

